/**************************
*Your name: Truong Pham
*CPSC 1021
*your Lab Section, F20: 001
*Your email: truongp@clemson.edu
*Section TA: John Choi
**************************/


#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include <cstdlib>

using namespace std;


typedef struct Employee{
	string lastName;
	string firstName;
	int birthYear;
	double hourlyWage;
}employee;

bool name_order(const employee& lhs, const employee& rhs);
int myrandom (int i) { return rand()%i;}


int main(int argc, char const *argv[]) {
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
  srand(unsigned (time(0)));


	employee employeeArr[10];

  /*Create an array of 10 employees and fill information from standard input with prompt messages*/
	//ask for 10 employee info
	for(int i = 0; i < 10; i++)
	{
		cout << "Employee #" << i + 1 << "\n\n";

		cout << "Enter last name: ";
		cin >> employeeArr[i].lastName;

		cout << "Enter first name: ";
		cin >> employeeArr[i].firstName;

		cout << "Enter birth year: ";
		cin >> employeeArr[i].birthYear;

		cout << "Enter hourly wage: ";
		cin >> employeeArr[i].hourlyWage;

		cout << "\n";
	}
  /*After the array is created and initialzed we call random_shuffle() see the
   *notes to determine the parameters to pass in.*/

	random_shuffle(&employeeArr[0], &employeeArr[10], myrandom);


	/*Build a smaller array of 5 employees from the first five cards of the array created
    *above*/


	//initalize new array to first five indexes
	employee empArrFive[5] = {employeeArr[0], employeeArr[1], employeeArr[2], employeeArr[3], employeeArr[4]};

    /*Sort the new array.  Links to how to call this function is in the specs
     *provided*/

	sort(&empArrFive[0], &empArrFive[5], name_order);

    /*Now print the array below */

	//range based loop to print out struct info
	for(auto n : empArrFive)
	{
		cout << setw(2) << "" << n.lastName;
		cout << setw(2) << ", " << n.firstName << endl;
		cout << setw(2) << " " << n.birthYear << endl;
		cout << setw(2) << setprecision(2) << "  " << n.hourlyWage << endl;

	}


  return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/
bool name_order(const employee& lhs, const employee& rhs) {

	return lhs.lastName < rhs.lastName;
}
